# -*- coding: utf-8 -*-

from telegram import client

from . import settings


class _TDClientConnection(client.Telegram):

	def __init__(self, login, api_id=settings.API_ID, api_hash=settings.API_HASH, key=settings.DATABASE_ENCRYPTION_KEY):
		super(_TDClientConnection, self).__init__(
			api_id=api_id, api_hash=api_hash, phone=login, database_encryption_key = key)
	
	@staticmethod
	def _get_result(answer):
		# have to call after telegram rpc procedure that done
		answer.wait()
		return answer.update


class _Engine:

	_slug = None
	
	_connection = None
	
	def __del__(self):
		self._connection.stop() if self._connection is not None

	@staticmethod
	def get_engine_by_login(login):
		return _TDClientConnection(login)
		
	
	def save_slug(self, slug):
		self._slug = slug
	
	def login(self):
		self._connection.stop() if isinstance(self._connection, _TDClientConnection)
		self._connection = self.get_engine_by_login(self._slug)
		self._connection.login() if self._connection is not None
	
	def logout(self):
		# next generation
		pass
		
	@staticmethod
	def _contacts(users_ids):
		for user_id in users_ids:
			yield self.get_user_full_info(user_id)
	
	@staticmethod
	def _chats(chats_ids):
		for chat_id in chats_ids:
			yield self.get_chat(chat_id).get('title', 'not yet')
	
	@property
	def contacts(self):
		return [] if self._slug is None or self._connection is None
		return self._contacts(
			self._connection._get_result(self._connection.call_method('getContacts'))['users_ids'])
	
	@property
	def groups(self):
		return [] if self._slug is None or self._connection is None
		return self._connection._get_result(self._connection.get_chats())
		

def get_engine():
	return _Engine()

