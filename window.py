# -*- coding: utf-8 -*-
from kivy import app, uix, lang

from . import engine, settings


lang.Builder.load_file('design.kv')


class HiddenTelegramLayoutMixin:

    @staticmethod
    def hide_widget(wid, dohide=True):
        if hasattr(wid, 'saved_attrs'):
            if not dohide:
                wid.height, wid.size_hint_y, wid.opacity, wid.disabled = wid.saved_attrs
                del wid.saved_attrs
        elif dohide:
            wid.saved_attrs = wid.height, wid.size_hint_y, wid.opacity, wid.disabled
            wid.height, wid.size_hint_y, wid.opacity, wid.disabled = 0, None, 0, True

class PageTelegramLayout(uix.pagelayout.Pagelayout, HiddenTelegramLayoutMixin):
    pass

class Application(app.App):

    engine = engine.get_engine()

    _last_telegram_slug = None
    
    _is_logined = False
    
    _is_contacts_opened = False
    
    _is_groups_opened = False
    
    _label = 'Logining'
    
    def build(self):
        return PageTelegramLayout()
    
    def save(self, slug):
        self._last_telegram_slug = slug
        if self._last_telegram_slug is not None:
            self.engine.save_slug(slug)
    
    @property
    def label(self):
        return self._label
        
    @property
    def button_content(self):
        if self._is_logined:
            return 'Logout'
        else:
            return 'Login'
    
    def login(self):
        if self._last_telegram_slug is self.engine.slug and self._last_telegram_slug is not None:
            self._label = self.engine.login()
    
    def logout(self):
        self._label = self.engine.logout()
        self._is_logined = False
        self._is_contacts_opened = False
        self._is_groups_opened = False

    
    def show_content(self):
        for contact in self.engine.contacts:
            self.contacts.add_node(uix.treeview.TreeViewLabel(text=contact))
            
    def show_groups(self):
        for group in self.engine.groups:
            self.group.add_node(uix.treeview.TreeViewLabel(text=group))